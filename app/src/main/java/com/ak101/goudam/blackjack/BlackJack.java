package com.ak101.goudam.blackjack;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;


public class BlackJack extends AppCompatActivity {

    private Button newButton;
    private Button hitButton;
    private Button stayButton;

    private Game game;

    private TextView messageTextView;
    private TextView deckTextView;
    private TextView playerScoreTextView;
    private TextView dealerScoreTextView;

    private TableLayout gameTableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_jack);

        //get a reference to the ui elements
        newButton = (Button) findViewById(R.id.newButton);
        hitButton = (Button) findViewById(R.id.hitButton);
        stayButton = (Button) findViewById(R.id.stayButton);

        messageTextView = (TextView) findViewById(R.id.messageTextView);
        deckTextView = (TextView) findViewById(R.id.deckTextView);
        playerScoreTextView = (TextView) findViewById(R.id.playerScoreTextView);
        dealerScoreTextView = (TextView) findViewById(R.id.dealerScoreTextView);

        gameTableLayout = (TableLayout) findViewById(R.id.gameTableLayout);

        game = new Game(this, messageTextView, deckTextView, playerScoreTextView, dealerScoreTextView, gameTableLayout);

        //register listeners for the interactive ui elements
        newButton.setOnClickListener(newButtonClickListener);
        hitButton.setOnClickListener(hitButtonClickListener);
        stayButton.setOnClickListener(stayButtonClickListener);
    }

    protected OnClickListener newButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            game.newGame();
        }
    };

    protected OnClickListener hitButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            game.hit();
        }
    };

    protected OnClickListener stayButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            game.stay();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_black_jack, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

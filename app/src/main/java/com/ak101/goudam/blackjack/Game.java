package com.ak101.goudam.blackjack;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Goudam on 10/5/15.
 */
public class Game {

    private boolean gameStart;
    private boolean freshGame;
    private boolean playerStay;
    private boolean playerBlackJack;
    private boolean dealerBlackJack;
    private boolean playerBusted;
    private boolean dealerBusted;
    private int gameHand;
    private int playerScore;
    private int dealerScore;
    private ArrayList<Card> playerHand;
    private ArrayList<Card> dealerHand;
    private final int BLACKJACK = 21;

    private Context context;
    private Resources res;
    private TextView messageTextView;
    private TextView deckTextView;
    private TextView playerScoreTextView;
    private TextView dealerScoreTextView;
    private TextView handTextView;
    private TextView resultTextView;
    private TextView dealerHandTextView;
    private TextView playerHandTextView;
    private TableLayout gameTableLayout;
    private LinearLayout dealerTableRow;
    private LinearLayout playerTableRow;

    private CardDeck cardDeck;

    Game(Context context, TextView messageTextView, TextView deckTextView, TextView playerScoreTextView, TextView dealerScoreTextView, TableLayout gameTableLayout){
        this.gameStart = false;
        this.freshGame = false;
        this.playerStay = false;
        this.playerBlackJack = false;
        this.dealerBlackJack = false;
        this.playerBusted = false;
        this.dealerBusted = false;
        this.gameHand = 0;
        this.playerScore = 0;
        this.dealerScore = 0;
        this.playerHand = new ArrayList<Card>();
        this.dealerHand = new ArrayList<Card>();
        this.context = context;
        this.res = context.getResources();
        this.messageTextView = messageTextView;
        this.deckTextView = deckTextView;
        this.playerScoreTextView = playerScoreTextView;
        this.dealerScoreTextView = dealerScoreTextView;
        this.gameTableLayout = gameTableLayout;
    }

    public void newGame(){
        if(gameStart){
            freshGame = true;
            showAlert(R.string.label_end_game, R.string.message_new_game);
        }
        else{
            cardDeck = new CardDeck();

            //New card deck if existing deck is empty or has less than 10 cards
            if(cardDeck.getDeckLength() == 0 || cardDeck.getDeckLength() < 10){
                cardDeck.setCards();
                cardDeck.shuffleCards();
            }

            freshGame = false;
            playerStay = false;
            playerBlackJack = false;
            dealerBlackJack = false;
            playerBusted = false;
            dealerBusted = false;
            gameHand = 0;
            playerScore = 0;
            dealerScore = 0;
            playerHand = new ArrayList<Card>();
            dealerHand = new ArrayList<Card>();

            gameTableLayout.removeAllViews();
            setMessageTextView();
            setPlayerScoreTextView();
            setDealerScoreTextView();
            makeNewDealView();

            gameStart = true;
        }
    }

    public void hit(){
        if(gameStart && !playerStay && getPlayerHandValue() < 21){
            Card card = dealCard();
            playerHand.add(card);
            makeCardGUI(playerTableRow, card.getCardImage());
            updatePlayerHand();
        }
    }

    public void stay(){
        if(gameStart && !playerStay){
            playerStay = true;
            dealerPlay();
        }
    }

    private void newGameHand(){
        playerStay = false;
        playerBlackJack = false;
        dealerBlackJack = false;
        playerBusted = false;
        dealerBusted = false;

        setMessageTextView();
        setPlayerScoreTextView();
        setDealerScoreTextView();
        resetHands();
        updateGameHand();
        makeNewDealView();
    }

    private void makeCardGUI(LinearLayout targetRow, int cardResource ){
        if(cardResource == -1){
            freshGame = true;
            showAlert(R.string.label_out_of_cards, R.string.message_new_game, true);
        }
        else {
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(res.getDrawable(cardResource));
            targetRow.addView(imageView);
        }
    }

    private Card dealCard(){
        Card newCard = cardDeck.getCard();
        setDeckTextView();
        return newCard;
    }

    private void dealFirstCards(){
        Card dealerCard1 = dealCard();
        dealerHand.add(dealerCard1);
        makeCardGUI(dealerTableRow, dealerCard1.getCardImage());

        Card dealerCard2 = dealCard();
        dealerHand.add(dealerCard2);
        makeCardGUI(dealerTableRow, R.drawable.b1fv);

        Card playerCard1 = dealCard();
        playerHand.add(playerCard1);
        makeCardGUI(playerTableRow, playerCard1.getCardImage());

        Card playerCard2 = dealCard();
        playerHand.add(playerCard2);
        makeCardGUI(playerTableRow, playerCard2.getCardImage());
    }

    private void makeNewDealView(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newDealView = inflater.inflate(R.layout.new_hand_view, null);

        ++gameHand;

        handTextView = (TextView) newDealView.findViewById(R.id.handTextView);
        resultTextView = (TextView) newDealView.findViewById(R.id.resultTextView);
        dealerHandTextView = (TextView) newDealView.findViewById(R.id.dealerTextView2);
        playerHandTextView = (TextView) newDealView.findViewById(R.id.playerTextView2);

        dealerTableRow = (LinearLayout) newDealView.findViewById(R.id.dealerCardLayout);
        playerTableRow = (LinearLayout) newDealView.findViewById(R.id.playerCardLayout);

        dealFirstCards();

        updateGameHand();
        dealerHandTextView.setText(String.format(res.getString(R.string.label_hand_count),"?"));
        updatePlayerHand();

        gameTableLayout.addView(newDealView, 0);
    }

    private void showDealerCard(){
        dealerTableRow.removeViewAt(1);
        makeCardGUI(dealerTableRow, dealerHand.get(1).getCardImage());
    }

    private int getDealerHandValue(){
        int dealerValue = 0;
        for(int i=0; i<dealerHand.size(); ++i){
            dealerValue += dealerHand.get(i).getCardValue();
        }

        return dealerValue;
    }

    private int getPlayerHandValue(){
        int playerValue = 0;
        for(int i=0; i<playerHand.size(); ++i){
            int cardValue = playerHand.get(i).getCardValue();
            //For Ace
            if(cardValue == 1){
                //Check if playerValue+11 is more than BLACKJACK
                //If yes, then add 1. Else, add 11;
                playerValue+= (playerValue+11 > BLACKJACK)?cardValue:11;
            }
            else{
                playerValue += cardValue;
            }
        }

        return playerValue;
    }

    private void resetHands(){
        playerHand = new ArrayList<Card>();
        dealerHand = new ArrayList<Card>();

    }

    private void updateGameHand(){
        handTextView.setText(String.format(res.getString(R.string.label_hand),gameHand));
    }

    private void updatePlayerHand(){
        int playerHandValue = getPlayerHandValue();
        if(playerHandValue > 21) {
            playerBusted = true;
            showDealerCard();
            decideWinner();
        }
        else{
            if(playerHandValue == 21){
                playerBlackJack = true;
                showDealerCard();
                updateDealerHand();
            }
            playerHandTextView.setText(String.format(res.getString(R.string.label_hand_count),playerHandValue));
        }
    }

    private void updateDealerHand(){
        int dealerHandValue = getDealerHandValue();
        if(playerBlackJack){
            if (dealerHandValue != 21) {
                dealerBusted = true;
            }
            else{
                dealerBlackJack = true;
            }
            decideWinner();
        }
        else {
            if (dealerHandValue > 21) {
                dealerBusted = true;
                decideWinner();
            } else {
                if (dealerHandValue >= 17) {
                    if (dealerHandValue == 21) {
                        dealerBlackJack = true;
                    }
                    decideWinner();
                } else {
                    dealerHit();
                }
            }
        }
        dealerHandTextView.setText(String.format(res.getString(R.string.label_hand_count), dealerHandValue));

    }

    private void dealerHit(){
        Card card = dealCard();
        dealerHand.add(card);
        makeCardGUI(dealerTableRow, card.getCardImage());
        updateDealerHand();
    }

    private void dealerStay(){
        updateDealerHand();
    }

    /**
     * Dealer plays
     */
    private void dealerPlay(){
        showDealerCard();
        if(getDealerHandValue() <= 16){
            dealerHit();
        }
        else{
            dealerStay();
        }
    }

    /**
     * Decides the winner of the hand
     */
    private void decideWinner(){
        int result = 0;
        if(playerBusted){
            setResultTextView(R.string.lose_message);
            result = R.string.lose_message;
            dealerScore++;
        }
        else if(playerBlackJack){
            if(dealerBlackJack){
                setResultTextView(R.string.push_message);
                result = R.string.push_message;
            }
            else{
                setResultTextView(R.string.win_message);
                result = R.string.win_message;
                playerScore++;
            }
        }
        else{
            if(dealerBusted){
                setResultTextView(R.string.win_message);
                result = R.string.win_message;
                playerScore++;
            }
            else {
                int dealerHandValue = getDealerHandValue();
                int playerHandValue = getPlayerHandValue();
                if(playerHandValue == dealerHandValue){
                    setResultTextView(R.string.push_message);
                    result = R.string.push_message;
                }
                else {
                    if(playerHandValue > dealerHandValue){
                        setResultTextView(R.string.win_message);
                        result = R.string.win_message;
                        playerScore++;
                    }
                    else{
                        setResultTextView(R.string.lose_message);
                        result = R.string.lose_message;
                        dealerScore++;
                    }
                }
            }

        }

        setPlayerScoreTextView();
        setDealerScoreTextView();
        showAlert(result, String.format(res.getString(R.string.message_new_hand),getPlayerHandValue(),
                getDealerHandValue()));

    }

    private void setMessageTextView(){
        messageTextView.setText(R.string.label_new_game);
    }

    private void setResultTextView(int message){
        resultTextView.setText(message);
    }

    private void setDeckTextView(){
        deckTextView.setText(""+cardDeck.getDeckLength());
    }

    private void setPlayerScoreTextView(){
        playerScoreTextView.setText(String.format(res.getString(R.string.label_player_score), playerScore));
    }

    private void setDealerScoreTextView(){
        dealerScoreTextView.setText(String.format(res.getString(R.string.label_dealer_score), dealerScore));
    }

    private void showAlert(int title, int message){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int button) {
                        if(freshGame){
                            freshGame = false;
                            gameStart = false;
                            newGame();
                        }
                        else{
                            newGameHand();
                        }
                    } //ok onclick
                });
        builder.setCancelable(true);
        builder.setNegativeButton(R.string.btn_cancel, null);
        builder.setMessage(message);

        AlertDialog confirmDialog = builder.create();
        confirmDialog.show();
    }

    private void showAlert(int title, int message, boolean noCancel){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int button) {
                        if (freshGame) {
                            freshGame = false;
                            gameStart = false;
                            newGame();
                        } else {
                            newGameHand();
                        }
                    } //ok onclick
                });
        builder.setNegativeButton(R.string.btn_cancel, null);
        builder.setMessage(message);

        AlertDialog confirmDialog = builder.create();
        confirmDialog.show();
    }

    private void showAlert(int title, String message){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int button) {
                        if (freshGame) {
                            freshGame = false;
                            gameStart = false;
                            newGame();
                        } else {
                            newGameHand();
                        }
                    } //ok onclick
                });
        builder.setCancelable(true);
        builder.setNegativeButton(R.string.btn_cancel, null);
        builder.setMessage(message);

        AlertDialog confirmDialog = builder.create();
        confirmDialog.show();
    }

}

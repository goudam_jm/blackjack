package com.ak101.goudam.blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Goudam on 10/5/15.
 */
public class CardDeck {

    private ArrayList<Card> cards;

    CardDeck(){
        this.cards = new ArrayList<Card>();
    }

    public Card getCard(){
        if(cards.size() > 0){
            return cards.remove(cards.size()-1);
        }
        return new Card(-1,-1);
    }

    public ArrayList<Card> getCards(){
        return cards;
    }

    public int getDeckLength(){
        return cards.size();
    }

    public void setCards(){
        //Add clubs to the deck
        cards.add(new Card(1, R.drawable.c1));
        cards.add(new Card(2, R.drawable.c2));
        cards.add(new Card(3, R.drawable.c3));
        cards.add(new Card(4, R.drawable.c4));
        cards.add(new Card(5, R.drawable.c5));
        cards.add(new Card(6, R.drawable.c6));
        cards.add(new Card(7, R.drawable.c7));
        cards.add(new Card(8, R.drawable.c8));
        cards.add(new Card(9, R.drawable.c9));
        cards.add(new Card(10, R.drawable.ct));
        cards.add(new Card(10, R.drawable.cj));
        cards.add(new Card(10, R.drawable.ck));
        cards.add(new Card(10, R.drawable.cq));

        //Add diamonds to the deck
        cards.add(new Card(1, R.drawable.d1));
        cards.add(new Card(2, R.drawable.d2));
        cards.add(new Card(3, R.drawable.d3));
        cards.add(new Card(4, R.drawable.d4));
        cards.add(new Card(5, R.drawable.d5));
        cards.add(new Card(6, R.drawable.d6));
        cards.add(new Card(7, R.drawable.d7));
        cards.add(new Card(8, R.drawable.d8));
        cards.add(new Card(9, R.drawable.d9));
        cards.add(new Card(10, R.drawable.dt));
        cards.add(new Card(10, R.drawable.dj));
        cards.add(new Card(10, R.drawable.dk));
        cards.add(new Card(10, R.drawable.dq));

        //Add hearts to the deck
        cards.add(new Card(1, R.drawable.h1));
        cards.add(new Card(2, R.drawable.h2));
        cards.add(new Card(3, R.drawable.h3));
        cards.add(new Card(4, R.drawable.h4));
        cards.add(new Card(5, R.drawable.h5));
        cards.add(new Card(6, R.drawable.h6));
        cards.add(new Card(7, R.drawable.h7));
        cards.add(new Card(8, R.drawable.h8));
        cards.add(new Card(9, R.drawable.h9));
        cards.add(new Card(10, R.drawable.ht));
        cards.add(new Card(10, R.drawable.hj));
        cards.add(new Card(10, R.drawable.hk));
        cards.add(new Card(10, R.drawable.hq));

        //Add spades to the deck
        cards.add(new Card(1, R.drawable.s1));
        cards.add(new Card(2, R.drawable.s2));
        cards.add(new Card(3, R.drawable.s3));
        cards.add(new Card(4, R.drawable.s4));
        cards.add(new Card(5, R.drawable.s5));
        cards.add(new Card(6, R.drawable.s6));
        cards.add(new Card(7, R.drawable.s7));
        cards.add(new Card(8, R.drawable.s8));
        cards.add(new Card(9, R.drawable.s9));
        cards.add(new Card(10, R.drawable.st));
        cards.add(new Card(10, R.drawable.sj));
        cards.add(new Card(10, R.drawable.sk));
        cards.add(new Card(10, R.drawable.sq));

    }

    public void shuffleCards(){
        long seed = System.nanoTime();
        Collections.shuffle(cards, new Random(seed));
    }
}

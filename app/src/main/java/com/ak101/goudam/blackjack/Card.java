package com.ak101.goudam.blackjack;

import android.graphics.drawable.Drawable;

/**
 * Created by Goudam on 10/5/15.
 */
public class Card {

    private int cardValue;
    private int cardImage;

    Card(int cardValue, int cardImage ){
        this.cardValue = cardValue;
        this.cardImage = cardImage;
    }

    public int getCardValue(){
        return cardValue;
    }

    public int getCardImage(){
        return cardImage;
    }
}
